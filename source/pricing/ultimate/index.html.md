---
layout: markdown_page
title: "Why Ultimate/Gold?"
---

## GitLab Ultimate/Gold

| Ultimate supports enterprise IT transformations to high-velocity delivery without sacrificing security, compliance or enterprise governance. Ultimate helps enterprises improve: <br> <br> **- Project Insights** <br> **- Portfolio Management** <br> **- Application Security**  <br> **- Compliance** <br> **- Operations** <br><br>  | [![GitLab Roadmap](/images/solutions/scaled-agile/gitlab_roadmap.png)](https://about.gitlab.com/images/solutions/scaled-agile/gitlab_roadmap.png) |

**Note:** Ultimate also includes 4 business hour support and a Technical Account Manager.

### Project Insights

| Get insights that matter for your projects to explore data such as triage hygiene, issues created/closed per a given period, average time for merge requests to be merged, amongst others. | [![Project Insights](/ee/user/project/insights/img/project_insights.png)](https://docs.gitlab.com/ee/user/project/insights/img/project_insights.png) |

| Specific Ultimate Features    | Value |
| --------- | ------------ |
| [Project Insights](https://docs.gitlab.com/ee/user/project/insights/) | Visualize project insights to improve developer efficiencies.   |


### Security

| Cybersecurity is a critical concern of every business leader.  Your applications MUST be secure. GitLab Ultimate weaves security into the pipeline to provide early and actionable feedback to the development team.  | [![Security Dashboards](/images/feature_page/screenshots/61-security-dashboard.png)](https://about.gitlab.com/images/feature_page/screenshots/61-security-dashboard.png) |

| Specific Ultimate Features    | Value |
| --------- | ------------ |
| [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/) | Evaluates the static code, checking for potential security issues.   |
| [Dynamic Application Security Testing](https://docs.gitlab.com/ee/user/application_security/dast/) | Analyzes the review application to identify potential security issues.  |
| [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) |  Evaluates the third-party dependencies to identify potential security issues.   |
| [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/) |  Analyzes Docker images and checks for potential security issues.  |
| [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#project-security-dashboard) | Visualize the latest security status for each project and across projects. |
| [*Security Metrics and Trends* (future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/6954)| *Metrics and historical data about how many vulnerabilities have been spotted, addressed, solved, and how much time was spent for the complete cycle.* |


### Portfolio Management

| Establish end to end visibility from Business Idea to delivering value. GitLab Ultimate, enables portfolio planning, tracking, and execution in one tool, that unifies the team to focus on delivering business value. | [![Epics](/images/feature_page/screenshots/51-epics.png)](https://about.gitlab.com/images/feature_page/screenshots/51-epics.png) |

| Specific Ultimate Features     | Value |
| --------- | ------------ |
| [Epics](https://docs.gitlab.com/ee/user/group/epics/) |  Organize, plan, and prioritize business ideas and initiatives. |
| [Roadmaps](https://docs.gitlab.com/ee/user/group/roadmap/) | Visualize the flow of business initiatives across time in order to plan when future features will ship.   |
| [*VSM Workflow Analytics*(future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/7269) | *Visualize the end to end value stream to identify and resolve bottlenecks.* |
| [*Risk Management* (future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/3978) | *Manage risk of epics not being completed on time.* |
| [*What-If Scenario Planning*(future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/3979) | *Visualize potential impact in the overall portfolio if you were to make a change.* |
| [*Roadmap Capacity Planning*(future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/6777) | *Visualize if future work is feasible from an effort perspective.* |

### Operations

| Kubernetes Cluster Health Monitoring - Track cluster CPU and Memory utilization. Keeping an eye on cluster resources can be important, if the cluster runs out of memory pods may be shutdown or fail to start. | ![Cluster Monitoring](https://docs.gitlab.com/ee/user/project/clusters/img/k8s_cluster_monitoring.png) |

| Specific Ultimate Features    | Value |
| --------- | ------------ |
| [Kubernetes Cluster Health Monitoring](https://docs.gitlab.com/ee/user/project/clusters/#monitoring-your-kubernetes-cluster-ultimate) | Track cluster CPU and Memory utilization. Keeping an eye on cluster resources can be important, if the cluster runs out of memory pods may be shutdown or fail to start.   |
| [Kubernetes Cluster Logs](https://docs.gitlab.com/ee/user/project/clusters/kubernetes_pod_logs.html) | View the logs of running pods in connected Kubernetes clusters so that developers can avoid having to manage console tools or jump to a different interface. |
| [App Performance Alerts](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#setting-up-alerts-for-prometheus-metrics-ultimate) | Respond to changes to your application performance with alerts for custom metrics. |
| [Incident Management](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#taking-action-on-incidents-ultimate) | Automatically open issues for alerts and customize using issue templates |
| [Tracing](https://docs.gitlab.com/ee/user/project/operations/tracing.html) | Get insight into the performance and health of a deployed application, tracking each function or microservice which handles a given request |
| [*Anomaly Alerts* (future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/3610) | *alerting based on deviation from the weekly mean.* |

### Compliance

| License compliance allows to track project dependencies for their licenses and approve or blacklist specific licenses. | ![License Compliance](https://docs.gitlab.com/ee/user/application_security/license_compliance/img/license_compliance_search_v12_3.png) |

| Specific Ultimate Features    | Value |
| --------- | ------------ |
| License Compliance |  Identify the presence of new software licenses included in your project. Approve or deny the inclusion of a specific license. |
| [*CD with SOC 2 Compliance*(future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/4120) | *Support SOC 2 compliance* |

### Other

| Other    | Value |
| --------- | ------------ |
| Guest Users|  Guest users don’t count towards the license count.  |


<center><a href="/sales" class="btn cta-btn orange margin-top20">Contact sales and learn more about GitLab Ultimate</a></center>
