---
layout: job_family_page
title: "People Operations Specialist"
---
<a id="specialist-requirements"></a>

### Responsibilities

- Provide backup / guidance to specialists in responding to emails in the people operations email and Slack channel to ensure extraordinary customer service to employees and leaders at GitLab.
- Maintain appropriate level of process, program, and policy knowledge in order to assist team members.
- Intake of questions, complaints and concerns.
- Escalating issues to People Business Partners as needed.
- Administration of the signing of our Code of Conduct (annual signing in February and reporting on this).
- Administer probation period process - liaising with managers, communicating with PBP’s, updating team members. Update probation periods in BambooHR and the handbook.
- Manage full cycle of the relocation process and liaise with other teams as necessary.
- Identify automation opportunities.
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the employee and/or manager’s experience.
- Announcing changes and improvements on the Company Call and related slack channels. 
- Engagement survey administration & implementation.  
- Exit and Stay interviews for Individual Contributors.
- Provide support and cover for Associates including completion of Onboarding issues and Offboarding issues. 
- Providing support to the Senior Specialist regarding contract changes and administration.
- Work with the ITOps team and video conferencing vendor to improve the experience for GitLab Team members and the team administering the call. 
- Engage with co-employer’s and PEO's on a regular basis, especially during onboarding within those regions. 
- Identify gaps during GitLab onboarding and co-employer processes.
- Complete ad-hoc projects, reporting, and tasks.
- Administering PTO Ninja. 

### Requirements

- The ability to work autonomously and to drive your own performance & development would be important in this role.
- Prior extensive experience in an HR or People Operations role.
- Clear understanding of HR laws in one or multiple countries where GitLab is active.
- Willingness to work odd hours when needed (for example, to call an embassy in a different continent).
- Excellent written and verbal communication skills.
- Exceptional customer service skills.
- Strong team player who can jump in and support the team on a variety of topics and tasks.
- Enthusiasm for and broad experience with software tools.
- Proven experience quickly learning new software tools.
- Willing to work with git and GitLab whenever possible.
- Willing to make People Operations as open and transparent as possible.
- Proven organizational skills with high attention to detail and the ability to prioritize.
- You share our [values](/handbook/values), and work in accordance with those values.
- The ability to work in a fast paced environment with strong attention to detail is essential.
- High sense of urgency and accuracy.
- Bachelor's degree or 2 years related experience.
- Experience at a growth-stage tech company is preferred.

## Levels

### Senior People Operations Specialist

- Maintain appropriate level of process, program, and policy knowledge in order to assist team members. 
- Partner with Legal / Tax on international employment and contractual requirements.
- Handle WBSO grant with the Director of Tax.
- Review new locations and work with PEO’s to update contracts. Update handbook appropriately. 
- Data retention tracking and compliance.
- Review anniversary gift feedback on an annual basis and work with the supplier to iterate on the experience and ordering process.
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the employee and/or manager’s experience.
- Announcing changes and improvements on the Company Call and related slack channels. 
- Work on onboarding people experience, values alignment during onboarding and improving onboarding through continuous iteration.
- Stay interviews for IC’s.
- Prepare separation agreements in partnership with People Business Partners.
- Partner with the People Operations Fullstack Engineer on automation and removing mundane tasks from Onboarding. 
- Iterate on the onboarding issue and collaborate with the overall team to keep improving onboarding. 
- Work with People Ops and the executive team to establish entities or co-employers in new countries as we scale.
- Work with the People Operations Fullstack Engineer on automation related options for call management and videco conferencing provider.
- Manage Vendor contracts.  
- Renewing or ending of contracts and working closely with Procurement on negotiations, and vendor selections.
- Coordinate with Finance on PEO payroll issue.
- Complete ad-hoc projects, reporting, and tasks.
- Collaborating with PTO Ninja on a monthly basis to ensure we continue to iterate and improve on the integration and feedback. 

### Team Lead People Operations

- Coach and mentor PeopleOps team to effectively address team member queries in line with our values. 
- Continually audit and monitor compliance with policies and procedures.  
- Address any gaps and escalate to Manager, People Operations where required.
- Drive continued automation and efficiency to enhance the employee experience and maintain our efficiency value. 
- Announcing changes and improvements on the Company Call and related slack channels.
- Driving a positive employee experience throughout their lifecycle with GitLab.
- Monthly report on trends from Stay Interviews.
- Quarterly report on trends from exit interview data.
- Partner with all relevant teams in GitLab and PEO's on conversions. 
- Manage vendor renewals and agreements. 
- Partner with Finance Business Partner for budgeting purposes.
- Complete ad-hoc projects, reporting, and tasks.

## Performance Indicators

Primary performance indicators for this role:

- [Onboarding TSAT > 4](/handbook/people-group/people-operations-metrics/#onboarding-enps)
- [Onboarding task completion < X (TBD)](/handbook/people-group/people-operations-metrics/#onboarding-enps)

### Hiring Process
- Qualified candidates will be invited to schedule a 30 minute screening call with one of our recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our People Operations Manager
- After that, candidates will be invited to schedule a 30 minute interview with members of the People Operations and People Partner teams
- After that, candidates will be invited to interview with the Director of People Operations
- Finally, our CPO may choose to conduct a final interview
